package com.markwu.weatherapi.models

import java.util.*

data class ThreeHourBlock(
    val dt: Long,
    val main: Main,
    val weather: ArrayList<Weather>,
    val clouds: Clouds,
    val wind: Wind,
    val sys: Sys,
    val dt_txt: String
)