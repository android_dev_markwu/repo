package com.markwu.weatherapi.models

data class Sys(
    val pod: String
)