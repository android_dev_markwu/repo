package com.markwu.weatherapi.retrofit

import com.markwu.weatherapi.models.WeatherResultModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

// http://api.openweathermap.org/data/2.5/forecast
//      ?q=detroit
//      &appid=64f7dbd5a9b577f25d17aa922f4f16ab
interface WeatherService {

    @GET("data/2.5/forecast")
    fun getFiveDayForecast(
        @Query("q") city: String,
        @Query("appid") appId: String
    ): Call<WeatherResultModel>

}