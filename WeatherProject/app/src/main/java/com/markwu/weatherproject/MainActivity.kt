package com.markwu.weatherproject

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.markwu.weatherproject.viewmodel.MainViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private val allSubscriptions by lazy { CompositeDisposable() }

    private lateinit var cityEditText: EditText
    private lateinit var forecastButton: Button
    private lateinit var errorTextView: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var weatherAdapter: WeatherAdapter
    private lateinit var mainViewModel: MainViewModel

    private lateinit var progressOverlay: FrameLayout

    companion object {
        const val TAG = "MainActivity"
        const val OVERLAY_ALPHA = 0.5f
        const val OVERLAY_ANIMATE_DURATION_MS = 200L
        const val SUCCESS_RESPONSE = "200"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cityEditText = findViewById(R.id.city_edit_text)
        forecastButton = findViewById(R.id.forecast_button)
        errorTextView = findViewById(R.id.error_text_view)
        recyclerView = findViewById(R.id.results_recycler_view)

        progressOverlay = findViewById(R.id.progress_overlay)

        weatherAdapter = WeatherAdapter()
        recyclerView.adapter = weatherAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        if (savedInstanceState != null) { // It's configuration change, restore results (if any) from view model
            mainViewModel.getWeatherResult()?.let {
                weatherAdapter.setNewResults(it)
            }
        }

        forecastButton.setOnClickListener {
            if (getCity() == "") {
                showError(getString(R.string.edit_text_hint))
                return@setOnClickListener
            }
            showProgressBar()
            errorTextView.visibility = View.INVISIBLE
            allSubscriptions.add(
                Observable.fromCallable { mainViewModel.getWeatherForecast(getCity()) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally {
                        hideProgressBar()
                    }
                    .subscribe({
                        if (it.cod == SUCCESS_RESPONSE) {
                            weatherAdapter.setNewResults(it)
                            errorTextView.visibility = View.INVISIBLE
                            recyclerView.scrollToPosition(0)
                        } else {
                            showError(it.message)
                        }
                    }, {
                        showError(it.message)
                    })
            )
        }
    }

    private fun getCity() = cityEditText.text.toString().trim()

    private fun showError(errorMessage: String?) {
        weatherAdapter.clearResults()
        errorTextView.text = errorMessage
        errorTextView.visibility = View.VISIBLE
    }

    /** Animate in an indeterminate progress bar while searches are occurring over the network. */
    private fun showProgressBar() {
        AndroidUtils.animateView(progressOverlay, View.VISIBLE, OVERLAY_ALPHA, OVERLAY_ANIMATE_DURATION_MS);
    }

    /** Hide the indeterminate progress bar when searches are complete. */
    private fun hideProgressBar() {
        AndroidUtils.animateView(progressOverlay, View.GONE, OVERLAY_ALPHA, OVERLAY_ANIMATE_DURATION_MS);
    }

    override fun onDestroy() {
        super.onDestroy()
        allSubscriptions.clear()
    }
}
