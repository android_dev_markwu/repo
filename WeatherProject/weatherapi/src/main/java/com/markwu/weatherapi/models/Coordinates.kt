package com.markwu.weatherapi.models

data class Coordinates(
    val lat: Double,
    val lon: Double
)