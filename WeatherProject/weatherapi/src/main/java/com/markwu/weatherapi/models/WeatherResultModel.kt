package com.markwu.weatherapi.models

import java.util.*

data class WeatherResultModel(
    val cod: String?,
    val message: String?,
    val cnt: Int,
    val list: ArrayList<ThreeHourBlock>?,
    val city: City?
)