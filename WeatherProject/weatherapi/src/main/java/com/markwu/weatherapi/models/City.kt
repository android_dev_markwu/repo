package com.markwu.weatherapi.models

data class City(
    val id: Long,
    val name: String,
    val coord: Coordinates,
    val country: String,
    val population: Long
)