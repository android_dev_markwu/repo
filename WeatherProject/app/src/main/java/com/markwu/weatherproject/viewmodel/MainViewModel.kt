package com.markwu.weatherproject.viewmodel

import androidx.lifecycle.ViewModel
import com.markwu.weatherapi.models.WeatherResultModel
import com.markwu.weatherapi.retrofit.ApiConstants
import com.markwu.weatherapi.retrofit.WeatherApi

/** ViewModels survive configuration changes.  API results will be stored here and restored on configuration change. */
class MainViewModel: ViewModel() {
    private var weatherResult: WeatherResultModel? = null

    /** Retrofit execute() runs on current thread, but this will be called from background thread via RxJava (Schedulers.io()). */
    fun getWeatherForecast(city: String): WeatherResultModel {
        val callWeatherForecast = WeatherApi.getWeatherApi().getFiveDayForecast(city, ApiConstants.APP_ID)
        val response = callWeatherForecast.execute()
        if (response.isSuccessful) {
            weatherResult = response.body()
            weatherResult?.let {
                return it
            }
        }
        return WeatherResultModel(response.code().toString(), response.message(), 0, ArrayList(), null)
    }

    fun getWeatherResult() = weatherResult

}