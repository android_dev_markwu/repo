package com.markwu.weatherproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.markwu.weatherapi.models.ThreeHourBlock
import com.markwu.weatherapi.models.WeatherResultModel
import kotlinx.android.synthetic.main.list_item_row.view.*

class WeatherAdapter : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {
    private var listThreeHourBlocks: MutableList<ThreeHourBlock> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = listThreeHourBlocks.size

    override fun onBindViewHolder(holder: WeatherAdapter.ViewHolder, position: Int) {
        holder.textViewDate.text = listThreeHourBlocks[position].dt_txt
        holder.textTempLow.text = listThreeHourBlocks[position].main.temp_min.toString()
        holder.textTempHigh.text = listThreeHourBlocks[position].main.temp_max.toString()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewDate: TextView = view.date_text_view
        val textTempLow: TextView = view.low_text_view
        val textTempHigh: TextView = view.high_text_view
    }

    fun clearResults() {
        listThreeHourBlocks.clear()
        notifyDataSetChanged()
    }

    fun setNewResults(weatherResultModel: WeatherResultModel) {
        listThreeHourBlocks.clear()
        weatherResultModel.list?.let {
            listThreeHourBlocks.addAll(it)
        }
        notifyDataSetChanged()
    }
}