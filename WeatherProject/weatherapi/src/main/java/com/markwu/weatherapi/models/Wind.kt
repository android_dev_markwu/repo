package com.markwu.weatherapi.models

data class Wind(
    val speed: Double,
    val deg: Double
)