package com.markwu.weatherapi.retrofit

object ApiConstants {
    const val APP_ID = "64f7dbd5a9b577f25d17aa922f4f16ab"
    const val BASE_URL = "https://api.openweathermap.org"
}