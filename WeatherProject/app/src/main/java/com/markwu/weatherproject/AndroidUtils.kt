package com.markwu.weatherproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

object AndroidUtils {
    /**
     * animateView - Use view animations to make view switching less jarring.
     * e.g., showing the screen loading overlay, cause the background to dim gradually.
     *
     * https://developer.android.com/training/animation/reveal-or-hide-view
     * https://stackoverflow.com/questions/18021148/display-a-loading-overlay-on-android-screen
     *
     * @param view         View to animate
     * @param toVisibility Visibility at the end of animation
     * @param toAlpha      Alpha at the end of animation
     * @param duration     Animation duration in ms
     */
    fun animateView(view: View, toVisibility: Int, toAlpha: Float, duration: Long) {
        val show = toVisibility == View.VISIBLE
        if (show) {
            view.alpha = 0f
        }
        view.visibility = View.VISIBLE
        view.animate()
            .setDuration(duration)
            .alpha(if (show) toAlpha else 0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.visibility = toVisibility
                }
            })
    }
}